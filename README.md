# kernel

Configure sysctl, limit.conf and disable transparent_hugepage

## Requirements

none

## Role Variables

### Sub modules
```
cts_role_kernel:
  sysctl: false
  limits: false
  thp: false
```

### sysctl

default variables
```
cts_role_kernel_sysctl:
  - { key: "net.ipv4.conf.default.rp_filter",           value: "1" }    # IP Spoofing protection
  - { key: "net.ipv4.conf.all.rp_filter",               value: "1" }    # IP Spoofing protection
  - { key: "net.ipv4.tcp_syncookies",                   value: "1" }    # Block SYN attacks
  - { key: "net.ipv4.tcp_max_syn_backlog",              value: "2048" } # Block SYN attacks
  - { key: "net.ipv4.tcp_synack_retries",               value: "2" }    # Block SYN attacks
  - { key: "net.ipv4.tcp_syn_retries",                  value: "5" }    # Block SYN attacks
  - { key: "net.ipv4.conf.all.log_martians",            value: "1" }    # Log Martians
  - { key: "net.ipv4.icmp_ignore_bogus_error_responses",value: "1" }    # Log Martians
  - { key: "net.ipv4.ip_forward",                       value: "0" }    # Disable routing
  - { key: "net.ipv4.conf.default.accept_redirects",    value: "0" }    # Ignore ICMP redirects
  - { key: "net.ipv4.conf.all.accept_redirects",        value: "0" }    # Ignore ICMP redirects
  - { key: "net.ipv6.conf.default.accept_redirects",    value: "0" }    # Ignore ICMP redirects
  - { key: "net.ipv6.conf.all.accept_redirects",        value: "0" }    # Ignore ICMP redirects
  - { key: "net.ipv4.conf.default.send_redirects",      value: "0" }    # Ignore send redirects
  - { key: "net.ipv4.conf.all.send_redirects",          value: "0" }    # Ignore send redirects
  - { key: "net.ipv4.conf.default.accept_source_route", value: "0" }    # Disable source packet routing
  - { key: "net.ipv4.conf.all.accept_source_route",     value: "0" }    # Disable source packet routing
  - { key: "net.ipv6.conf.default.accept_source_route", value: "0" }    # Disable source packet routing
  - { key: "net.ipv6.conf.all.accept_source_route",     value: "0" }    # Disable source packet routing
  - { key: "net.ipv4.tcp_timestamps",                   value: "0" }    # Provide protection against wrapped sequence numbers
  - { key: "net.ipv4.icmp_echo_ignore_broadcasts",      value: "1" }    # Ignore ICMP broadcast requests
  - { key: "net.ipv4.tcp_tw_recycle",                   value: "1" }    # recycle TIME-WAIT connections
  - { key: "net.ipv4.tcp_tw_reuse",                     value: "1" }    # allow recycle TIME-WAIT connections
  - { key: "net.ipv6.conf.all.disable_ipv6",            value: "1" }    # disable ipv6
  - { key: "vm.oom_dump_tasks",                         value: "1" }    # Enables a system-wide task dump to be produced
  - { key: "vm.panic_on_oom",                           value: "1" }    # kernel panic when oomkiler
  - { key: "kernel.panic",                              value: "60"}    # reboot after 60sec when kernel panic
```

### exemple of limit for elasticsearch
```
cts_role_kernel_limits:
  - { domain: 'elasticsearch', limit_type: 'soft', limit_item: nofile,  value: 65535 }
  - { domain: 'elasticsearch', limit_type: 'hard', limit_item: nofile,  value: 65535 }
  - { domain: 'elasticsearch', limit_type: 'soft', limit_item: memlock, value: unlimited }
  - { domain: 'elasticsearch', limit_type: 'hard', limit_item: memlock, value: unlimited }
```

## Dependencies

none

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - kernel
```

